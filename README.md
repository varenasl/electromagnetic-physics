# Física Electromagnética
La física electromagnética, es la rama de la física que estudia
los fenómenos provocados por el magnetismo que es generado por la
corriente eléctrica. En este blog se explican los conceptos más
basicos para el estudio de esta ciencia, los cuales son la energía
electroestática y la corriente eléctrica.

En este blog se presentan conceptos básicos como:
- Corriente Eléctrica
- Energía potencial eléctrica
- Potencial Eléctrico
- Trabajo Eléctrico
- Diferencia de Potencial