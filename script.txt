https://html5up.net/phantom

https://buffer.com/library/free-images/

https://www.mathjax.org/#demo

https://www.youtube.com/watch?v=sMjs0Vr6KQQ





Energía Electroestática



- Energía potencial eléctrica o trabajo:

https://youtu.be/sMjs0Vr6KQQ?t=36

Supongamos que tenemos una carga positiva y una carga de prueba en el campo eléctrico de la carga fuente en una superficie gaussiana. La energía potencial eléctrica
o trabajo se define como la energía necesaria para mantener una carga eléctrica de prueba dentro un campo eléctrico de carga fuente. Para complementar este concepto, en palabras
más comunes, la energía potencial eléctrica es la energia necesaria para mantener la carga de prueba dentro de la superficie gaussiana, sí que dicha carga se mueva, es decir, jugando en
contra de las fuerzas de atracción o repulsión.



U = (KQq)/r



U = Energia Potencial eléctrica

K = Constante eléctrica de Coulomb

Q = Carga de fuente

q = Carga de prueba

r = distancia entre cargas



Unidades--------------- J





- Potencial Eléctrico:

https://youtu.be/sMjs0Vr6KQQ?t=162

Cuando tenemos una carga positiva y una carga de prueba que puede estar situada en varios puntos en diferentes superficies gaussianas. Podemos analizar que la carga de prueba
sigue siendo la misma, pero ahora nos interesa medir su potencia en función de su posición en el campo eléctrico, entendiendo que en cada superficie gaussiana habrá siempre una intensidad
distinta del campo eléctrico. Así pues, el potencial eléctrico es la energia potencial eléctrica o trabajo por cada unidad de carga dentro de un campo electrico.



V = U/q = KQ/r



V = Potencial eléctrico

K = Constante eléctrica de Coulomb

Q = Carga fuente

r = distancia entre la carga fuente y su superficie gaussiana



Unidades----------J/C = V



- Diferencia de potencial:

https://youtu.be/sMjs0Vr6KQQ?t=315

En la figura 3 se muestra una carga fuente, y una carga de prueba que se mueve en varias superficies gaussianas. La diferencia de potencial nos permite cuantificar
la energia eléctrica que se desarrolla para transportar una carga de prueba de un punto a otro dentro de la región del campo eléctrico de la carga fuente. Generalmente
la diferencia de potencial es calculada para cargas positivas que se mueven de un punto de mayor o otro de menor potencial eléctrico.



DeltaVsubAB = VsubA - VsubB



VsubA = Potencial eléctrico de la carga de prueba en una superficie gaussiana A

VsubB = Potencial eléctrico de la carga de prueba en una superficie gaussiana B



Unidades ---------- J/C = V





- Trabajo Eléctrico:

https://youtu.be/sMjs0Vr6KQQ?t=479

En la figura 4 se muestra una carga fuente y otra carga de prueba, que se mueve hacia una superficie gaussiana diferente siguiendo una trayectoria. El trabajo eléctrico,
se define como la energia necesaria para mover una carga de prueba de un lugar a otro dentro de un campo eléctrico. Para calcular el trabajo eléctrico, podemos usar la fórmula



WsubE = -DeltaV * q



- = Indica que sin importar la trayectoria de la carga siempre se realiza el trabajo en contra de las fuerzas de atracción o repulsión

DeltaV = La diferencia de potencial

q = La carga eléctrica de la partícula de prueba



Unidades ---------------- J



- Corriente Electrica:

https://www.youtube.com/watch?v=kYwNj9uauJ4

https://www.britannica.com/science/electric-current

La corriente eléctrica es el flujo de partículas cargadas tales como electrones o iones, que se mueven a través de un conductor eléctrico o un espacio.
Es definida como la tasa neta de flujo de carga eléctrica a través de una superficie o dentro de un control de volumen. La corriente eléctrica en un cable, donde los portadores de carga son electrones, es la medida de la cantidad de carga que pasa por cualquier punto del cable
por unidad de tiempo.



Las abreviaciones para esas unidades son 1 Ampere = 1 Coulomb/segundo



